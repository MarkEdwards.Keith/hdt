#include <iostream>
#include <vector>

// Hàm tìm đoạn con không giảm dài nhất của dãy số
std::vector<int> findLongestNonDecreasingSubarray(const std::vector<int>& array) {
    int start = 0; // Vị trí bắt đầu của đoạn con không giảm
    int end = 0; // Vị trí kết thúc của đoạn con không giảm
    int maxLength = 1; // Độ dài của đoạn con không giảm hiện tại
    int currentLength = 1; // Độ dài của đoạn con không giảm đang xét

    // Duyệt qua từng phần tử trong dãy
    for (int i = 1; i < array.size(); i++) {
        // Nếu phần tử hiện tại lớn hơn hoặc bằng phần tử trước đó, tăng độ dài đoạn con không giảm
        if (array[i] >= array[i - 1]) {
            currentLength++;
        }
        // Nếu phần tử hiện tại nhỏ hơn phần tử trước đó, kiểm tra xem đoạn con không giảm hiện tại có dài hơn đoạn con không giảm đã tìm được trước đó hay không
        else {
            if (currentLength > maxLength) {
                maxLength = currentLength;
                start = i - maxLength;
                end = i - 1;
            }
            currentLength = 1; // Đặt lại độ dài của đoạn con không giảm đang xét
        }
    }

    // Kiểm tra xem đoạn con không giảm hiện tại có dài hơn đoạn con không giảm đã tìm được trước đó hay không (xảy ra khi đoạn con không giảm là phần cuối của dãy)
    if (currentLength > maxLength) {
        maxLength = currentLength;
        start = array.size() - maxLength;
        end = array.size() - 1;
    }

    // Tạo một vector chứa đoạn con không giảm dài nhất
    std::vector<int> result;
    for (int i = start; i <= end; i++) {
        result.push_back(array[i]);
    }

    return result;
}

int main() {
    // Nhập dãy số
    int N;
    std::cout << "Nhập số phần tử của dãy: ";
    std::cin >> N;

    std::vector<int> array(N);
    std::cout << "Nhập các phần tử của dãy: ";
    for (int i = 0; i < N; i++) {
        std::cin >> array[i];
    }

    // Tìm đoạn con không giảm dài nhất
    std::vector<int> longestSubarray = findLongestNonDecreasingSubarray(array);

    // In kết quả
    std::cout << "Đoạn con không giảm dài nhất: ";
    for (int i = 0; i < longestSubarray.size(); i++) {
        std::cout << longestSubarray[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
