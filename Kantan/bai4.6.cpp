#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include <algorithm> //lay ham sort
using namespace std;
struct mathang
{
    string mamathang;
    string tenmathang;
    int soluong;
    double giaban;
};
bool sosanh(const mathang &a, const mathang &b)
{
    if(a.soluong>b.soluong)
        return true;
    if(a.soluong<b.soluong)
        return false;
    return a.giaban>b.giaban;
}
int main()
{
    ifstream finp;
    ofstream fout;
    finp.open("mathangin.txt");
    fout.open("mathangout.txt");
    fout<<"";
    int n;
    finp>>n;
    vector<mathang> danhsach(n);

    for(int i=0;i<n;i++)
    {
        finp >> danhsach[i].mamathang;
        finp.ignore();
        getline(finp,danhsach[i].tenmathang);
        finp>>danhsach[i].soluong;
        finp>>danhsach[i].giaban;
    }
    sort(danhsach.begin(),danhsach.end(),sosanh);
    for(int i=0;i<n;i++)
    {
        fout << danhsach[i].tenmathang<<"\t"
            << danhsach[i].soluong<<"\t"
            << danhsach[i].giaban<<"\n";
    }
    finp.close();
    fout.close();
    return 0;
}