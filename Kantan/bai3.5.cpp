#include <iostream>
#include <string>

std::string encodeString(const std::string& inputString) {
    std::string encodedString = "";
    for (char c : inputString) {
        if (isalpha(c)) {
            char upperC = toupper(c);
            if (upperC >= 'A' && upperC <= 'C') {
                encodedString += "2";
            } else if (upperC >= 'D' && upperC <= 'F') {
                encodedString += "3";
            } else if (upperC >= 'G' && upperC <= 'I') {
                encodedString += "4";
            } else if (upperC >= 'J' && upperC <= 'L') {
                encodedString += "5";
            } else if (upperC >= 'M' && upperC <= 'O') {
                encodedString += "6";
            } else if (upperC >= 'P' && upperC <= 'S') {
                encodedString += "7";
            } else if (upperC >= 'T' && upperC <= 'V') {
                encodedString += "8";
            } else if (upperC >= 'W' && upperC <= 'Z') {
                encodedString += "9";
            }
        } else {
            encodedString += c;
        }
    }
    return encodedString;
}

int main() {
    std::string inputString;
    std::cout << "Nhập chuỗi cần mã hóa: ";
    std::getline(std::cin, inputString);

    std::string encodedString = encodeString(inputString);
    std::cout << "Chuỗi đã mã hóa: " << encodedString << std::endl;

    return 0;
}
