#include<iostream>

using namespace std;

int phepcong(int x, int y)
{
    return x+y;
}
int pheptru(int x, int y)
{
    return x-y;
}

int pheptoan(int x, int y, int(*phep_toan)(int, int))
{
    int z;
    z=(*phep_toan)(x,y);
}

int main()
{
    int m,n;
    int (*pheptru_ptr)(int,int)=pheptru;
    m=pheptoan(15,20,&phepcong);
    n=pheptoan(40,20,pheptru_ptr);
    cout<<m<<endl<<n<<endl;
    return 0;
}