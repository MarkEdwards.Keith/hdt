#include<bits/stdc++.h>
using namespace std;
//class lop
//thuoc tinh attribute
// phuong thuc method,behavior
//object doi tuong
// encapsulation dong goi trong class
//acess modifier PVHD private public protected
// thuoc tinh de private
// phuong thuc de public
// constructor ham khoi tao
// destructor ham huy
// con tro This
// static
// friend funcion , friend class
class sinhvien
{
    private:
        string id,ten,ns;
        double gpa;
        static int dem;
    public:
        sinhvien();
        sinhvien(string,string,string,double);
        double getgpa();// lay thuoc tu prive ma k truy cap truc tiep
        void setgpa(double);//chinh sua thuoc tinh
        void nhap();
        void xuat();
        void tangdem();
        // int getdem(){
        //     return dem;
        // }
        int getdem();
        friend void inthongtin(sinhvien);
};
void sinhvien::nhap()
{
    ++dem;
    this->id = "sv"+string(3-to_string(dem).length(),'0')+to_string(dem);
    cout<<"nhap ten : ";getline(cin,ten);
    cout<<"ns : ";cin>>ns;
    cout<<"gpa : "; cin>>gpa;
    cin.ignore();
}
void sinhvien::xuat(){
    cout<<id<<" "<<ten<<" "<<ns<<" "<<fixed<<setprecision(2)<<gpa<<endl;
}
sinhvien::sinhvien(){
    //cout<<"sinh vien \n";
    id=ten=ns="";
    gpa=0;
}
sinhvien::sinhvien(string id, string ten, string ns, double gpa)
{
    this->id=id;
    this->ten=ten;
    this->ns=ns;
    this->gpa=gpa;
}
double sinhvien::getgpa(){
    return this->gpa;
}

bool compare(sinhvien a, sinhvien b){
    return a.getgpa()>b.getgpa();
}
void sinhvien::setgpa(double gpa){
    this->gpa=gpa;
}
int sinhvien::dem=0;
void sinhvien::tangdem(){
    ++dem;
}
int sinhvien::getdem(){
    return dem;
}
int main()
{
    sinhvien x;
    x.nhap();
    x.xuat();
    sinhvien y;
    y.nhap();
    y.xuat();
    return 0;
}