#include<iostream>
#include<conio.h>

using namespace std;
void nhap(int *p, int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<"a["<<i<<"] = ";
        cin>>*(p+i);
    }
}
void xuat(int *p, int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<*(p+i)<<"\t";
    }
    cout<<endl;
}
int tong(int *p, int n)
{
    int sum=0;
    for(int i=0;i<n;i++)
    {
        if(*(p+i)%2==0||*(p+i)%3==0)
        {
            sum+=*(p+i);
        }
    }
    return sum;
}
int ktnt(int *p, int a)
{
    int count=0;
    for(int i=1;i<=a;i++)
    {
        if(*(p+i)%i==0)
        {
            count++;
        }
    }
    if(count==2)
        return 1;
    return 0;
}
int slnt(int *p, int a)
{
    int count=0;
    for(int i=0;i<a;i++)
    {
        if(ktnt(p+i,a)==1)
        {
            count++;
        }
    }
    return count;
}
int main()
{
    int n,*p;
    cout<<"n = ";
    cin>>n;
    p = new int[n];
    nhap(p,n);
    xuat(p,n);
    cout<<tong(p,n)<<endl;
    cout<<slnt(p,n)<<endl;
    delete p;
    getch();
    return 0;
}