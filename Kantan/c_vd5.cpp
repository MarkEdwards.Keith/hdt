#include<bits/stdc++.h>
using namespace std;
//class lop
//thuoc tinh attribute
// phuong thuc method,behavior
//object doi tuong
// encapsulation dong goi trong class
//acess modifier PVHD private public protected
// thuoc tinh de private
// phuong thuc de public
// constructor ham khoi tao
// destructor ham huy
// con tro This
// static
// friend funcion , friend class
// nguyen van   A => Nguyen Van A
//forward declaration
//operator overloading
// cout: ostream
// cin: istream

class sinhvien
{
    private:
        string id,ten,namsinh;
        double gpa;
    public:
        sinhvien();
        friend istream& operator >> (istream &in,sinhvien& a);
        friend ostream& operator << (ostream &out, sinhvien a);
        //bool operator < (sinhvien a);
        friend bool operator < (sinhvien a, sinhvien b);
};
// bool sinhvien::operator < (sinhvien a){
//     return this->gpa=a.gpa;
// }
bool operator < (sinhvien a, sinhvien b){
    return a.gpa<b.gpa;
}
sinhvien::sinhvien(){
    id=ten=namsinh="";
    gpa=0;
}
istream& operator >> (istream &in,sinhvien& a){
    cout<<"nhap id : ";
    in>> a.id;
    cout<<"nhap ten : ";in.ignore();
    getline(in,a.ten);
    cout<<"nam sinh : ";
    in>>a.namsinh;
    cout<<"gpa : ";
    in>>a.gpa;
    return in;
}
ostream& operator << (ostream &out, sinhvien a){
    out<<a.id<<" "<<a.ten<<" "<<a.namsinh<<" "<<a.gpa<<endl;
    return out;
}
int main()
{
    sinhvien x,y;
    cin>>x>>y;
    if(x<y) cout<<"yes";
    else    cout<<"NO";
    return 0;
}