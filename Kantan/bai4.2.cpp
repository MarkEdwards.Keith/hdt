#include<iostream>
#include<conio.h>
using namespace std;
void nhap(int *p, int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<"a["<<i<<"] = ";
        cin>>*(p+i);
    }
}
void xuat(int *p, int n)
{
    for(int i=0;i<n;i++)
    {
        cout<<*(p+i)<<"\t";
    }
    cout<<endl;
}
void timdoan(int *p , int a)
{
    int lengthmax=1;
    int lengthcurent=1;
    int start,end;
    for(int i=0;i<a;i++)
    {
        if(*(p+i)!=*(p+i+1))
        {
            lengthcurent++;
        }
        else
        {
            if(lengthcurent>lengthmax)
            {
                lengthmax=lengthcurent;
                start=i-lengthmax+1;
                end =i;
            }
            lengthcurent=1;
        }
    }

    if (lengthcurent> lengthmax) 
    {
        lengthmax = lengthcurent;
        start = a - lengthmax+1;
        end = a - 1;
    }
   // cout<<lengthmax<<endl;
    //cout<<start<<endl<<end<<endl;
    for(int i=start;i<=end;i++)
    {
        cout<<*(p+i)<<"\t";
    }
    cout<<endl;
}
int main()
{
    int n,*p;
    cout<<"n = ";
    cin>>n;
    p = new int[n];
    nhap(p,n);
    xuat(p,n);
    timdoan(p,n);
    delete p;
    getch();
    return 0;
}