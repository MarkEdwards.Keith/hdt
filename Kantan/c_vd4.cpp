#include<bits/stdc++.h>
using namespace std;
//class lop
//thuoc tinh attribute
// phuong thuc method,behavior
//object doi tuong
// encapsulation dong goi trong class
//acess modifier PVHD private public protected
// thuoc tinh de private
// phuong thuc de public
// constructor ham khoi tao
// destructor ham huy
// con tro This
// static
// friend funcion , friend class
// nguyen van   A => Nguyen Van A
//forward declaration
//operator overloading

class giaovien;
class sinhvien;
class giaovien{
    private:
        string khoa;
    public:
        void update(sinhvien&);
};
class sinhvien
{
    friend class giaovien;
    private:
        string id,ten,ns;
        double gpa;
        static int dem;
    public:
        sinhvien();
        sinhvien(string,string,string,double);
        double getgpa();// lay thuoc tu prive ma k truy cap truc tiep
        void setgpa(double);//chinh sua thuoc tinh
        void nhap();
        void xuat();
        void tangdem();
        // int getdem(){
        //     return dem;
        // }
        int getdem();
        friend void inthongtin(sinhvien);
        friend void chuanhoa(sinhvien&);
};
void chuanhoa(sinhvien &a){
    string res="";
    stringstream ss(a.ten);
    string token;
    while(ss>>token){
        res+=toupper(token[0]);
        for(int i=1; i<token.length();i++){
            res+= tolower(token[i]);
        }
        res+=" ";
    }
    res.erase(res.length()-1);
    a.ten=res;
}
void sinhvien::nhap()
{
    ++dem;
    this->id = "sv"+string(3-to_string(dem).length(),'0')+to_string(dem);
    cout<<"nhap ten : ";getline(cin,ten);
    cout<<"ns : ";cin>>ns;
    cout<<"gpa : "; cin>>gpa;
    cin.ignore();
}
void sinhvien::xuat(){
    cout<<id<<" "<<ten<<" "<<ns<<" "<<fixed<<setprecision(2)<<gpa<<endl;
}
sinhvien::sinhvien(){
    //cout<<"sinh vien \n";
    id=ten=ns="";
    gpa=0;
}
sinhvien::sinhvien(string id, string ten, string ns, double gpa)
{
    this->id=id;
    this->ten=ten;
    this->ns=ns;
    this->gpa=gpa;
}
double sinhvien::getgpa(){
    return this->gpa;
}

bool compare(sinhvien a, sinhvien b){
    return a.getgpa()>b.getgpa();
}
void sinhvien::setgpa(double gpa){
    this->gpa=gpa;
}
int sinhvien::dem=0;
void sinhvien::tangdem(){
    ++dem;
}
int sinhvien::getdem(){
    return dem;
}
void inthongtin(sinhvien a){
    cout<<a.id<<" "<<a.ten<<endl;
}
void giaovien::update(sinhvien& x){
    x.gpa=3.20;
}
int main()
{
    sinhvien x;
    x.nhap();
    giaovien y;
    y.update(x);
    x.xuat();
    return 0;
}