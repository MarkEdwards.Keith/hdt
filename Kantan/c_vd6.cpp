#include<bits/stdc++.h>
using namespace std;

class sophuc{
    private:
        int thuc,ao;
    public:
        friend istream& operator >>(istream&in,sophuc&a);
        friend ostream& operator <<(ostream&out, sophuc a);
        //sophuc operator + (sophuc a);
        friend sophuc operator + (sophuc a, sophuc b);
};
istream& operator >>(istream&in,sophuc&a){
    in>>a.thuc>>a.ao;
    return in;
}
ostream& operator <<(ostream&out, sophuc a){
    out<<a.thuc<<" "<<a.ao;
    return out;
}
// sophuc sophuc:: operator + (sophuc a){
//     sophuc tong;
//     tong.thuc = this->thuc + a.thuc;
//     tong.ao   = this->ao + a.ao;
//     return tong;
// }
sophuc operator + (sophuc a, sophuc b){
    sophuc tong;
    tong.thuc=a.thuc+b.thuc;
    tong.ao = a.ao+b.ao;
    return tong;
}
int main()
{
    sophuc x,y;
    cin>>x>>y;
    cout<<x<<" "<<y;
    sophuc tong =  x+y;
    cout<<tong<<endl;
    return 0;
}