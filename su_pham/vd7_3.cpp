#include<bits/stdc++.h>
using namespace std;


class PhanSo{
    double a,b;
    public:
        PhanSo(){a=b=0;}
        PhanSo(double a,double b){
            this->a=a;
            this->b=b;
        }
        int ucln(int x, int y){
            if(y==0) return x;
            return ucln(y,y%x);
        }
        void rut_gon(){
            double gcd = ucln(a,b);
            a /=gcd;
            b/=gcd;
        }
        PhanSo operator + (const PhanSo& ps)const{
            double tsm= a*ps.b+b*ps.a;
            double msm = b*ps.b;
            PhanSo tong(tsm,msm);
            tong.rut_gon();
            return tong;
        }
        void display()const{
                cout<<a<<"/"<<b<<endl;
        }
        friend istream&operator >>(istream&,PhanSo&);
        friend ostream&operator <<(ostream&,PhanSo&);
};
istream&operator >>(istream& in,PhanSo& x){
    in>>x.a>>x.b;
}
ostream&operator <<(ostream& out,PhanSo& x){
    cout<<x.a<<"/"<<x.b<<endl;
}
int main(){
    PhanSo x;
    PhanSo y;
    cin>>y;
    cout<<y;
    cin>>x;
    cout<<x;
    PhanSo tong=x.operator+(y);
    tong.display();
    return 0;
}