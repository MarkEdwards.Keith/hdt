#include<bits/stdc++.h>
using namespace std;

class SinhVien{
    private:
      string ma,ten,ns,lop;
      double gpa;
    public:
        SinhVien(){
            lop=ma=ten=ns="";
            gpa=0;
        };
        SinhVien(string ten, string ns, double gpa){
            this->ten = ten;
            this->ns  = ns;
            this->gpa    = gpa;
        };
        friend void nhap(SinhVien&);
        friend void in(SinhVien);
};
void nhap(SinhVien &a){
    a.ma="B20DCCN001";
    getline(cin,a.ten);
    cin>>a.lop;
    cin>>a.ns;
    cin>>a.gpa;
    if(a.ns[2]!='/') a.ns="0"+a.ns;
    if(a.ns[5]!='/') a.ns.insert(3,"0");
}
void in(SinhVien a){
    cout<<a.ma<<" "<<a.ten<<" "<<a.lop<<" "<<a.ns<<" "<<fixed<<setprecision(2)<<a.gpa<<endl;
}
#define a() a
int main()
{
    SinhVien a();// sinhvien a
    nhap(a);
    in(a);
    return 0;
}