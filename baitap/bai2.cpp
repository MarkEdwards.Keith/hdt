#include<bits/stdc++.h>
using namespace std;
using ll= long long;
class phanso{
    private:
        ll tu,mau;
    public:
        phanso(ll tu, ll mau);
        friend istream& operator >>(istream&,phanso&);
        friend ostream& operator <<(ostream&,phanso);
        void rutgon(); 
        friend phanso operator + (phanso, phanso);
};
istream& operator >>(istream&in,phanso&a){
    in>>a.tu>>a.mau;
    return in;
}
ostream& operator <<(ostream&out,phanso a){
    out<<a.tu<<"/"<<a.mau;
    return out;
}
ll ucln(ll a, ll b)
{
    if(b==0) return a;
    return ucln(b,a % b);
}
phanso::phanso(ll tu, ll mau)
{
    this->tu=tu;
    this->mau=mau;
}
void phanso::rutgon()
{
    ll g=ucln(tu,mau);
    tu = tu/g;
    mau= mau/g;
}
ll bcnn(ll a, ll b){
    return a/ucln(a,b)*b;
}
phanso operator + (phanso a, phanso b){
    phanso tong(1,1);
    ll mc=bcnn(a.mau,b.mau);
    tong.tu = mc/a.mau*a.tu + mc/b.mau*b.tu;
    tong.mau = mc;
    ll g = ucln(tong.tu,tong.mau);
    tong.tu/=g; tong.mau/=g;
    return tong;
}
int main()
{
    phanso p(1,1),q(1,1);
    cin>>p>>q;
    cout<<p+q;
    return 0;
}