#include<bits/stdc++.h>
using namespace std;
using ll= long long;
class phanso{
    private:
        ll tu,mau;
    public:
        friend istream& operator >>(istream&,phanso&);
        friend ostream& operator <<(ostream&,phanso);
        void rutgon(); 
};
istream& operator >>(istream&in,phanso&a){
    in>>a.tu>>a.mau;
    return in;
}
ostream& operator <<(ostream&out,phanso a){
    out<<a.tu<<"/"<<a.mau;
    return out;
}
ll ucln(ll a, ll b)
{
    if(b==0) return a;
    return ucln(b,a % b);
}

void phanso::rutgon()
{
    ll g=ucln(tu,mau);
    tu = tu/g;
    mau= mau/g;
}
int main()
{
    phanso x;
    cin>>x;
    x.rutgon();
    cout<<x;
    return 0;
}