#include<bits/stdc++.h>
using namespace std;

class SinhVien{
    private:
      string ma,ten,lop,email;
    public:
        SinhVien(){
            lop=ma=ten=email="";
        };
        friend istream& operator >> (istream&,SinhVien&);
        friend ostream& operator << (ostream&,SinhVien);
        // string getlop(){
        //     return this->lop;
        // };
        bool operator < (SinhVien a){
            return this->lop<a.lop;
        };
};
istream& operator >> (istream&in,SinhVien&a){
    in>>a.ma;
    in.ignore();
    getline(in,a.ten);
    in>>a.lop>>a.email;
    return in;
}
ostream& operator << (ostream&out,SinhVien a){
    out<<a.ma<<" "<<a.ten<<" "<<a.lop<<" "<<a.email<<endl;
    return out;
}
// bool cmp(SinhVien a, SinhVien b){
//     return a.getlop()<b.getlop();
// }
int main()
{
    // int n; cin>>n;
    // SinhVien a[n];
    // for(int i=0;i<n;i++) cin>>a[i];
    // sort(a,a+n);
    // //for(int i=0;i<n;i++) //cout<<a[i];
    // for(SinhVien x : a){
    //     cout<<x;
    // }
    vector<SinhVien> v;
    SinhVien tmp;
    while(cin>>tmp){
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    for(SinhVien x: v){
        cout<<x;
    }
    return 0;
}