#include<bits/stdc++.h>
using namespace std;

class person{
    private:
        string name,addres;
    public:
        //funcion overiding
        // void nhap(){
        //     getline(cin,name);
        //     getline(cin,addres);
        // }
        person(string name, string addres){
            this->name=name;
            this->addres=addres;
        }
        string getname(){
            return this->name;
        }
        string getadd(){
            return addres;
        }
        void setname(string name){
            this->name=name;
        }
        void setadd(string addres){
            this->addres=addres;
        }
        void in(){
            cout<<name << " "<<addres<<" ";
        }

};
class sinhvien: public person{
    private:
        float gpa;
    public:
        // void nhap(){
        //     person::nhap();
        //     cin>>gpa;
        // }
        sinhvien(string name, string addres, float gpa):person(name,addres){
            this->gpa=gpa;
        }
        float getgpa(){
            return gpa;
        }
        void setgpa(float gpa){
            this->gpa = gpa;
        }
        void in(){
            person::in();
            cout<<fixed<<setprecision(2)<<gpa<<endl;
        }
};

int main()
{
    sinhvien x("nguyen van a","hai duong",3.5);
    //x.nhap(); // x.person::nhap();
    x.in();
    return 0;
}